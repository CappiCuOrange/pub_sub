#include "ros/ros.h"
// этот файл создастся автоматически из файла Letter.msg
#include "letter_msgs/Letter.h"

int main(int argc, char **argv)
{
  /**
   * Функция ros::init() инициализирует подсистему ROS.
   * Первыми двумя параметрами передаются параметры командной строки т.к. ноды ROS
   * используют свои параметры (см. http://wiki.ros.org/Nodes)
   * третьим параметром задается имя ноды, четвертым могут быть переданы опции инициализации.
   */
  ros::init(argc, argv, "twilight_sparkle");

  /**
   * При помощи NodeHandle осуществляется коммуникация с подсистемой ROS.
   * Первый созданный NodeHandle полностью инициализирует ноду,
   * а последний уничтоженный NodeHandle завершает ноду.
   */
  ros::NodeHandle n;

  /**
   * При помощи функции advertise() (англ. извещать, информировать, оповещать; уведомлять;)
   * можно сказать ROS, что мы хотим опубликовать сообщения в топик с определенным именем.
   * Функция возвращает объект Publisher, который позволяет публиковать сообщения в топик,
   * вызвая функцию publish(). Топик автоматически закроется как только все копии объекта
   * Publisher будут уничтожены. Вторым параметром передается размер буфера.
   */
  ros::Publisher spike_pub = n.advertise<letter_msgs::Letter>("spike", 1000);

  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
  {
    /**
     * Это объект сообщения, мы заполняем его данными и публикуем.
     */
    letter_msgs::Letter letter;

    letter.text = "Dear Princess Celestia";
    letter.num = count;

    /**
     * Функция publish() посылает сообщения. Параметром передается объект сообщения.
     * Тип сообщения должен совпадать с тем, который был указан при вызове advertise().
     */
    spike_pub.publish(letter);

    ROS_INFO("Spike sends %lu letters to princess", letter.num);

    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }
  return 0;
}

