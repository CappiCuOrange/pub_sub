#include "ros/ros.h"
// этот файл создастся автоматически из файла Letter.msg
#include "letter_msgs/Letter.h"

void chatterCallback(const letter_msgs::Letter::ConstPtr& letter)
{
  ROS_INFO("A letter from Twilight: [%s] %lu", letter->text.c_str(), letter->num);
}

int main(int argc, char **argv)
{
  /**
   * Функция ros::init() инициализирует подсистему ROS.
   * Первыми двумя параметрами передаются параметры командной строки т.к. ноды ROS
   * используют свои параметры (см. http://wiki.ros.org/Nodes)
   * третим параметром задается имя ноды, четвертым могут быть переданы опции инициализации.
   */
  ros::init(argc, argv, "princess_celestia");

  /**
   * При помощи NodeHandle осуществляется коммуникация с подсистемой ROS.
   * Первый созданный NodeHandle полностью инициализирует ноду,
   * а последний уничтоженный NodeHandle завершает ноду.
   */
  ros::NodeHandle n;

  /**
   * При помощи функции subscribe() можно сказать ROS, что вы хотите получать сообщения
   * из определенного топика. Сообщения будут переданы в callback функцию chatterCallback.
   * Функция возвращает объект Subscriber, который нужно хранить пока вы не захотите отписаться.
   * Вторым параметром передается размер буфера.
  */
  ros::Subscriber spike_sub = n.subscribe("spike", 1000, chatterCallback);

  /**
   * Функция ros::spin() не дает программе завершиться, она инициализирует бесконечный цикл,
   * при этом все callback функции будут вызваны в этом же (главном) потоке.
   * Функция завершится при нажатии Ctrl-C, или если нода будет завершена из вне мастером.
   */
  ros::spin();
  return 0;
}

