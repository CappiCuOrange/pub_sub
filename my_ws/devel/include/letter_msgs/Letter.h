// Generated by gencpp from file letter_msgs/Letter.msg
// DO NOT EDIT!


#ifndef LETTER_MSGS_MESSAGE_LETTER_H
#define LETTER_MSGS_MESSAGE_LETTER_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace letter_msgs
{
template <class ContainerAllocator>
struct Letter_
{
  typedef Letter_<ContainerAllocator> Type;

  Letter_()
    : text()
    , num(0)  {
    }
  Letter_(const ContainerAllocator& _alloc)
    : text(_alloc)
    , num(0)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _text_type;
  _text_type text;

   typedef int64_t _num_type;
  _num_type num;





  typedef boost::shared_ptr< ::letter_msgs::Letter_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::letter_msgs::Letter_<ContainerAllocator> const> ConstPtr;

}; // struct Letter_

typedef ::letter_msgs::Letter_<std::allocator<void> > Letter;

typedef boost::shared_ptr< ::letter_msgs::Letter > LetterPtr;
typedef boost::shared_ptr< ::letter_msgs::Letter const> LetterConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::letter_msgs::Letter_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::letter_msgs::Letter_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace letter_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'letter_msgs': ['/home/cappicu/ros/my_ws/src/pub_sub/letter_msgs/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::letter_msgs::Letter_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::letter_msgs::Letter_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::letter_msgs::Letter_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::letter_msgs::Letter_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::letter_msgs::Letter_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::letter_msgs::Letter_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::letter_msgs::Letter_<ContainerAllocator> >
{
  static const char* value()
  {
    return "8f374d0b248f5b35ac85071f75de722c";
  }

  static const char* value(const ::letter_msgs::Letter_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x8f374d0b248f5b35ULL;
  static const uint64_t static_value2 = 0xac85071f75de722cULL;
};

template<class ContainerAllocator>
struct DataType< ::letter_msgs::Letter_<ContainerAllocator> >
{
  static const char* value()
  {
    return "letter_msgs/Letter";
  }

  static const char* value(const ::letter_msgs::Letter_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::letter_msgs::Letter_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string text\n\
int64 num\n\
\n\
";
  }

  static const char* value(const ::letter_msgs::Letter_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::letter_msgs::Letter_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.text);
      stream.next(m.num);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Letter_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::letter_msgs::Letter_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::letter_msgs::Letter_<ContainerAllocator>& v)
  {
    s << indent << "text: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.text);
    s << indent << "num: ";
    Printer<int64_t>::stream(s, indent + "  ", v.num);
  }
};

} // namespace message_operations
} // namespace ros

#endif // LETTER_MSGS_MESSAGE_LETTER_H
