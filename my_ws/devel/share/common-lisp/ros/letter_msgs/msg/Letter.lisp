; Auto-generated. Do not edit!


(cl:in-package letter_msgs-msg)


;//! \htmlinclude Letter.msg.html

(cl:defclass <Letter> (roslisp-msg-protocol:ros-message)
  ((text
    :reader text
    :initarg :text
    :type cl:string
    :initform "")
   (num
    :reader num
    :initarg :num
    :type cl:integer
    :initform 0))
)

(cl:defclass Letter (<Letter>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Letter>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Letter)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name letter_msgs-msg:<Letter> is deprecated: use letter_msgs-msg:Letter instead.")))

(cl:ensure-generic-function 'text-val :lambda-list '(m))
(cl:defmethod text-val ((m <Letter>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader letter_msgs-msg:text-val is deprecated.  Use letter_msgs-msg:text instead.")
  (text m))

(cl:ensure-generic-function 'num-val :lambda-list '(m))
(cl:defmethod num-val ((m <Letter>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader letter_msgs-msg:num-val is deprecated.  Use letter_msgs-msg:num instead.")
  (num m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Letter>) ostream)
  "Serializes a message object of type '<Letter>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'text))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'text))
  (cl:let* ((signed (cl:slot-value msg 'num)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Letter>) istream)
  "Deserializes a message object of type '<Letter>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'text) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'text) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'num) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Letter>)))
  "Returns string type for a message object of type '<Letter>"
  "letter_msgs/Letter")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Letter)))
  "Returns string type for a message object of type 'Letter"
  "letter_msgs/Letter")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Letter>)))
  "Returns md5sum for a message object of type '<Letter>"
  "8f374d0b248f5b35ac85071f75de722c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Letter)))
  "Returns md5sum for a message object of type 'Letter"
  "8f374d0b248f5b35ac85071f75de722c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Letter>)))
  "Returns full string definition for message of type '<Letter>"
  (cl:format cl:nil "string text~%int64 num~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Letter)))
  "Returns full string definition for message of type 'Letter"
  (cl:format cl:nil "string text~%int64 num~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Letter>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'text))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Letter>))
  "Converts a ROS message object to a list"
  (cl:list 'Letter
    (cl:cons ':text (text msg))
    (cl:cons ':num (num msg))
))
