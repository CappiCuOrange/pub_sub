
(cl:in-package :asdf)

(defsystem "letter_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Letter" :depends-on ("_package_Letter"))
    (:file "_package_Letter" :depends-on ("_package"))
  ))