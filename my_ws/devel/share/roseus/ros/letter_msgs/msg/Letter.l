;; Auto-generated. Do not edit!


(when (boundp 'letter_msgs::Letter)
  (if (not (find-package "LETTER_MSGS"))
    (make-package "LETTER_MSGS"))
  (shadow 'Letter (find-package "LETTER_MSGS")))
(unless (find-package "LETTER_MSGS::LETTER")
  (make-package "LETTER_MSGS::LETTER"))

(in-package "ROS")
;;//! \htmlinclude Letter.msg.html


(defclass letter_msgs::Letter
  :super ros::object
  :slots (_text _num ))

(defmethod letter_msgs::Letter
  (:init
   (&key
    ((:text __text) "")
    ((:num __num) 0)
    )
   (send-super :init)
   (setq _text (string __text))
   (setq _num (round __num))
   self)
  (:text
   (&optional __text)
   (if __text (setq _text __text)) _text)
  (:num
   (&optional __num)
   (if __num (setq _num __num)) _num)
  (:serialization-length
   ()
   (+
    ;; string _text
    4 (length _text)
    ;; int64 _num
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _text
       (write-long (length _text) s) (princ _text s)
     ;; int64 _num
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _num (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _num) (= (length (_num . bv)) 2)) ;; bignum
              (write-long (ash (elt (_num . bv) 0) 0) s)
              (write-long (ash (elt (_num . bv) 1) -1) s))
             ((and (class _num) (= (length (_num . bv)) 1)) ;; big1
              (write-long (elt (_num . bv) 0) s)
              (write-long (if (>= _num 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _num s)(write-long (if (>= _num 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _text
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _text (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int64 _num
#+(or :alpha :irix6 :x86_64)
      (setf _num (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _num (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get letter_msgs::Letter :md5sum-) "8f374d0b248f5b35ac85071f75de722c")
(setf (get letter_msgs::Letter :datatype-) "letter_msgs/Letter")
(setf (get letter_msgs::Letter :definition-)
      "string text
int64 num


")



(provide :letter_msgs/Letter "8f374d0b248f5b35ac85071f75de722c")


