# CMake generated Testfile for 
# Source directory: /home/cappicu/ros/my_ws/src
# Build directory: /home/cappicu/ros/my_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(pub_sub/letter_msgs)
subdirs(pub_sub/celestia)
subdirs(pub_sub/twilight)
